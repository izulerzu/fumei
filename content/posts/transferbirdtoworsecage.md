---
title: "No, the bird did not get freed."
date: 2022-11-05T15:16:38Z
draft: false
---

But got transferred to a worse birdcage.

You've probably heard the recent news. Elon Musk has bought the social media platform Twitter, after a long back-and-forth communication, some disagreements, and a lawsuit that probably just made him buy the platform rather than risk himself legally.

Entering the HQ in SF with a sink, and with a bio that says "Chief Twit" (Actually, at the time of writing this, it's now "Twitter Complaint Hotline Operator"), he was ready to take on the platform.

**Or was he?**

# You just bought a broken platform.

Elon, honestly I wonder what the f*** is going on in your brain sometimes, you just bought a platform that is in a disastrous state right now, sure it is still successful and mostly still used around the world but, you want to solve problems that are not what a usual techie would know how to fix, **because the platform's problems are not *technical*, they are *political***, you are dealing with a multitude of people with different ideologies, some very influential people, and some who are looking for truth and justice, and some who just wants to see chaos.

And, it's difficult, in fact very complicated, probably impossible. And now that you own the platform, you are now responsible for them, and I challenge you to fix them.

You need to fix the problems that the platform has been and **STILL IS** suffering currently, homophobia, transphobia, islamophobia, anti-Semitism, racism, ableism, sexism, the list goes on and on, and just because I didn't list some things here doesn't mean that it's not a problem and if I did list, every single thing this blog post would probably reach 2400 words halfway through doing it.

### Broken moderation? who cares! unsuspend all accounts.

Also, Twitter's already suffering from moderating problems, and you want to make it worse. That's what at least I learned from what you said about going to unsuspend every account regardless of what it is about, which means you already failed to solve the major problem of the platform because guess what? people don't want to use a platform riddled with terrible people. Wow! Who would've thought?

### You want a checkmark? Longer videos? pay 8$.

Ah yes, who cares about all the above problems? Let's care about the checkmark first, A.K.A a fresh stream of revenue for the company.

Oh, don't worry, it's not the only stream of revenue he's going to make, especially with advertisers slowly giving up the platform.

Money >>>>>>>>>>>>>>

### Major layoffs.

Elon Musk at the time of writing this laid off about half of Twitter and is probably gonna do it more soon, and with many terrible stories telling how this went down, the most insane one was probably where he laid off [the entire accessibility team](https://twitter.com/gerardkcohen/status/1588584461398347777).

Doesn't matter what you work on there, important or not, you are most likely getting fired, that's what I learned.

# Conclusion

And with all of that? Do you still think that Elon Musk would be an excellent king of Twitter? with him mostly making the platform pay to use (it's going to be worse over time, trust me), not caring about fixing the platform's major problems, and major layoffs that affect even certain strong positions, for me I don't think the future is looking good for the platform, sure it won't die immediately, but it's going to have a very slow death unless something happens that will make it fasten its death by like 50 times.

I don't know, in my opinion, we should all move to [mastodon](https://joinmastodon.org/), having social media as a protocol is much more merciful than having it under a company that can be taken over by a rich dumb person, especially someone who is CEO for 3 companies at the same time, which makes me think... is being CEO, not a very demanding or important of a job? ¯\\\_(ツ)\_/¯

Anyways, [welcome to hell, Elon](https://www.theverge.com/2022/10/28/23428132/elon-musk-twitter-acquisition-problems-speech-moderation), I hope you will enjoy it, oh and [follow me on mastodon](https://mastodon.online/@izulerzu) just in case, and below are some links that'll help you get up and going.

[What is the Fediverse?](https://en.wikipedia.org/wiki/Fediverse)

[I'm interested in mastodon, I want to learn more about it.](https://joinmastodon.org/)

[I'm still confused about both, can you tell me about it more or in a simpler detail?](https://fedi.tips/mastodon-and-the-fediverse-beginners-start-here/)

[I'm sold, any tips?](https://fedi.tips/)

[Alright, now I need to follow some people.](https://fedi.directory/)